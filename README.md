# apocalypse_roller

A Slack app used for playing Powered by the Apocalypse games in Slack  

## Requirements
Python3 + flask  

## Running
First, clone the repository:  

`git clone http://gitlab.com/CyanBlob/apocalypse_roller`  

Then, navigate into the cloned directory:  

`cd apocalypse_roller`  

Export environment variables (read by flask + the bot to determine behavior at runtime)  
`export SLACK_VERIFICATION_TOKEN=<token>`  
`export SLACK_TEAM_ID=<team_id>`  
`export FLASK_APP=src/app.py`  
`export FLASK_ENV=development`  

To run the bot locally (for testing with Postman, etc):  

`flask run`  

To enable remote use/testing (for production):  

`flask run --host=0.0.0.0`  

## Testing
To test the bot, I recommend postman. Start the bot, and configure postman to send POST requests to 127.0.0.1 (or another IP on the same network if running the bot on a different device)  
Add data to the body of the request. Set 'token' and `team_id` to whatever you exported above. Also set `user_name' and 'user_id`. These don't need to be anything specific, but keep them consistent across requests  
Lastly, set `text` to be the command you're testing. It will be the same as what you type into Slack, such as `/debt list` and then send the request