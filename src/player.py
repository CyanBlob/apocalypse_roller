from enum import IntEnum
class Stats(IntEnum):
    ID        = 0
    NAME      = 1
    BLOOD     = 2
    HEART     = 3
    MIND      = 4
    SPIRIT    = 5
    MORTALITY = 6
    NIGHT     = 7
    POWER     = 8
    WILD      = 9
    
class Player:
    def __init__(self, stringData):
        splitData = stringData.split(' ')

        self.name      = splitData[0]
        self.blood     = splitData[1]
        self.heart     = splitData[2]
        self.mind      = splitData[3]
        self.spirit    = splitData[4]
        self.mortality = splitData[5]
        self.night     = splitData[6]
        self.power     = splitData[7]
        self.wild      = splitData[8]

    name = ""
    blood = heart = mind = spirit = 0
    mortality = night = power = wild = 0
    harm = 0
    holds = 0
