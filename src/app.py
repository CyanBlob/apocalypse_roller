import os
import sys
import random
import re

from flask import abort, Flask, jsonify, request

from player import Player, Stats
from db import Database

app = Flask(__name__)

players = dict()
db = Database()

def rollDice():
    #roll two dice to match the bell-curve of a real 2d6 roll
    return random.randint(1, 6) + random.randint(1, 6)

def isRequestValid(request):
    validIds = os.environ['SLACK_VERIFICATION_TOKEN'].split(",")
    isTokenValid = request.form["token"] in validIds

    return isTokenValid

def isInt(val):
    try:
        int(val)
        return True
    except ValueError:
        return False

@app.route('/roll', methods=['POST'])
def roll():
    #print(request.form, file=sys.stderr)
    if not isRequestValid(request):
        abort(400)

    rollVal = rollDice()

    return jsonify(
            response_type='in_channel',
            text=request.form["user_name"] + " rolled: " + str(rollVal)
            )

@app.route('/edit', methods=['POST'])
def edit():
    if not isRequestValid(request):
        abort(400)

    player = Player(request.form["text"])

    players[request.form["user_id"]] = player

    query =  "REPLACE INTO PLAYERS VALUES ('" + request.form["user_id"] + "', "
    query += "'" + str(player.name) + "', "
    query += str(player.blood) + ", "
    query += str(player.heart) + ", "
    query += str(player.mind) + ", "
    query += str(player.spirit) + ", "
    query += str(player.mortality) + ", "
    query += str(player.night) + ", "
    query += str(player.power) + ", "
    query += str(player.wild) + ", "
    query += str(player.harm) + ", "
    query += str(player.holds)
    query += ")"

    db.runQuery(query)

    return jsonify({
        "response_type": "in_channel",
        "text": "Usage: /edit <name> <blood> <heart> <mind> <spirit> <mortality> <night> <power> <wild>",
        "attachments": [
            {
                "text": "Created user " + players[request.form["user_id"]].name + " with stats: \n "
                + "Blood: " + str(players[request.form["user_id"]].blood) + "\n"
                + "Heart: " + str(players[request.form["user_id"]].heart) + "\n"
                + "Mind: " + str(players[request.form["user_id"]].mind) + "\n"
                + "Spirit: " + str(players[request.form["user_id"]].spirit) + "\n"
                + "Mortality: " + str(players[request.form["user_id"]].mortality) + "\n"
                + "Night: " + str(players[request.form["user_id"]].night) + "\n"
                + "Power: " + str(players[request.form["user_id"]].power) + "\n"
                + "Wild: " + str(players[request.form["user_id"]].wild)
                }
            ]
        })

def tokenize(string):
    # remove extra spaces
    string = re.sub(' +', ' ', string)
    return string.strip().lower().split(" ")

def statToString(stat):
    if (stat == Stats.BLOOD):
        return "Blood"
    elif (stat == Stats.HEART):
        return "Heart"
    elif (stat == Stats.MIND):
        return "Mind"
    elif (stat == Stats.SPIRIT):
        return "Spirit"
    elif (stat == Stats.NIGHT):
        return "Night"
    elif (stat == Stats.MORTALITY):
        return "Mortality"
    elif (stat == Stats.POWER):
        return "Power"
    elif (stat == Stats.WILD):
        return "Wild"
    else:
        return "<none>"

def formatStatRoll(name, rollVal, modifier, stat, override):
    response = str(name) + " rolled: " + str(rollVal+modifier+override) + " (" + str(rollVal) + " "
    response += "+"
    if (override != 0):
        response += " " + str(modifier) + " " + statToString(stat) + ", + " + str(override) + ")"
    else:
        response += " " + str(modifier) + " " + statToString(stat) + ")"

    return response

# TODO: remove repeated code
def doChannel(num, array):
    if not isRequestValid(request):
        abort(400)

    playerData = db.getPlayer(request.form["user_id"])

    if playerData is None:
        return jsonify(
                response_type='in_channel',
                text="Player not found. Do /edit first!"
                )

    rollVal = int(rollDice())
    modifier = int(playerData[Stats.SPIRIT])

    selected = []
    for _i in range(0, num):
        selected.append(array.pop(random.randint(0, len(array) - 1)))

    response = str(playerData[Stats.NAME]) + " rolled: " + str(rollVal+modifier) + " (" + str(rollVal) + " "
    response += "+"
    response += " " + str(modifier) + " " + statToString(Stats.SPIRIT) +")"
    response += "\nSpells: " + str(selected)

    return jsonify(
            response_type='in_channel',
            text=response)

def doBasicRoll(stat):
    if not isRequestValid(request):
        abort(400)

    text = ""
    override = 0
    try:
        text = tokenize(request.form["text"])
        try:
            override = int(text[0])
        except ValueError:
            abort(400)
    except:
        pass

    playerData = db.getPlayer(request.form["user_id"])

    if playerData is None:
        return "Player not found. Do /edit first!"

    rollVal = int(rollDice())
    modifier = int(playerData[stat])

    return formatStatRoll(playerData[Stats.NAME], rollVal, modifier, stat, override)

def doFactionRoll():
    if not isRequestValid(request):
        abort(400)

    splitText = tokenize(request.form["text"])
    command = splitText[0]

    override = 0
    if (len(splitText) > 1):
        try:
            override = int(splitText[1])
        except:
            abort(400)

    if (command == "mortality"):
        stat = Stats.MORTALITY
    elif (command == "night"):
        stat = Stats.NIGHT
    elif (command == "wild"):
        stat = Stats.WILD
    elif (command == "power"):
        stat = Stats.POWER
    else:
        stat = None

    playerData = db.getPlayer(request.form["user_id"])

    if playerData is None:
        return "Player not found. Do /edit first!"


    rollVal = rollDice()
    modifier = 0 if stat == None else int(playerData[stat]) 
    response = playerData[Stats.NAME] + " rolled: " + str(int(rollVal)+int(modifier)+override) + " (" + str(rollVal) + " "
    response += "+"
    if (override != 0):
        response += " " + str(modifier) + " " + statToString(stat) + ", + " + str(override) + ")"
    else:
        response += " " + str(modifier) + " " + statToString(stat) + ")"

    return response

@app.route('/blood', methods=['POST'])
def blood():
    response = doBasicRoll(Stats.BLOOD)
    response = ":muscle::woman-lifting-weights::gun:" + response
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/spirit', methods=['POST'])
def spirit():
    response = ":unicorn_face::fairy::mermaid: " + doBasicRoll(Stats.SPIRIT)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/heart', methods=['POST'])
def heart():
    response = "two_hearts::heartbeat::two_hearts: " + doBasicRoll(Stats.HEART)
    return jsonify(
            response_type='in_channel',
            text=response
            )


@app.route('/mind', methods=['POST'])
def mind():
    response = ":brain::nerd_face::raising_hand: " + doBasicRoll(Stats.MIND)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/unleash', methods=['POST'])
def unleash():
    response = ("When you unleash an attack on someone, roll with blood. "
            "On a hit, you inflict harm as established and choose one below:.\n\n*Inflict terrible harm \n*Take something from them."
            "\n\n On a 7-9, choose one from below as well: \n*They inflict harm on you \n*You find yourself in a bad spot \n") + doBasicRoll(Stats.BLOOD)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/escape', methods=['POST'])
def escape():
    response = ("When you takea advantage of an opening to escape a situation, roll with Blood."
            "On a hit, you get away. On a 10+, choose 1. On a 7-9, choose 2:"
            "\n-You suffer harm during your escape" 
            "\n-You end up in another dangerous situation"
            "\n-You leave something important behind "
            "\n-You owe someone a debt for your escape "
            "\n-You give into your base nature and mark corruption. \n") + doBasicRoll(Stats.BLOOD)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/persuade', methods=['POST'])
def persuade():
    response = ("When you persuade an NPC through seduction, promise, threats, or because the MC told you this was a persuade roll, roll with Heart. "
            "On a hit, they do what you ask. On a 7-9, they modify the deal or demand a debt in return."
            "\nIf you cash in a Debt you have with them before rolling, you may add +3 to your roll.\n") + doBasicRoll(Stats.HEART)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/figure', methods=['POST'])
def figure():
    response = ("When you try to figure someone out, roll with Mind. On a hit, hold 2. On a 7-9, they hold 1 on you as well.\n"
            "\n-Who's pulling your character's strings?"
            "\n-What's your character's beef with _____?"
            "\n-What's your character hoping to get from _____?"
            "\n-How could I get your character to _____?"
            "\n-What does your character worry might happen?"
            "\n-How could I put your character in my debt."
            "\n\nIf you're in their faction, you may ask an additional question, even on a miss.\n") + doBasicRoll(Stats.MIND)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/mislead', methods=['POST'])
def mislead():
    response = ("When you try to mislead, distract, or trick someone, roll with Mind. On a hit, they are fooled, at least for a moment. On a 10+, pick 3. On a 7-9, pick 2."
            "\n\n-You create an opportunity."
            "\n-You excpose a weakness or flaw."
            "\n-You confuse them for some time."
            "\n You avoid further entanglement.\n") + doBasicRoll(Stats.MIND)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/keepcool', methods=['POST'])
def keepcool():
    response= ("Y'all know we play this one fast and loose. On a 10+, all is well. Anything less, well...\n") +doBasicRoll(Stats.SPIRIT)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/letitout', methods=['POST'])
def letitout():
    response= ("When you let it out, roll with Spirit. On a hit, choose 1 and mark corruption. On a 10+, ignore the corruption or choose another from the list below."
            "\n\n-Take +1 forward on your next roll."
            "\n-Extend your senses, supernatural or otherwise."
            "\n-Frighten, intimidate, or impress your opposition."
            "\n-Take definite hold of something vulnerable or exposed. \n" ) +doBasicRoll(Stats.SPIRIT)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/channel', methods=['POST'])
def channel():
    response= doChannel(3, ["Tracking", "Elementalism", "Memory Wipe",
        "Shielding", "Veil", "Teleport", "Hex"])

    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/night', methods=['POST'])
def night():
    response = ":ghost::wolf::vampire: " + doBasicRoll(Stats.NIGHT)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/mortality', methods=['POST'])
def mortality():
    response = ":gun::mag::older_adult:" + doBasicRoll(Stats.MORTALITY)
    return jsonify(
            response_type='in_channel',
            text=response
            )

@app.route('/wild', methods= ['POST'])
def wild():
    response = ":fairy::black_nib::dragon:" + doBasicRoll(Stats.WILD)
    return jsonify(
            response_type= 'in_channel',
            text=response
            )

@app.route('/power', methods= ['POST'])
def power():
    response = ":eye::male_mage::female_mage:" + doBasicRoll(Stats.POWER)
    return jsonify(
            response_type= 'in_channel',
            text=response
            )

@app.route('/lendhand', methods=['POST'])
def lendhand():
    response= "When you lend a hand or get in the way after a PC has rolled, roll with their Faction. On a hit, give them a +1 or a -2 to their roll. On a 7-9, you expose yourself to danger, entaglement, or cost.\n" + doFactionRoll()
    return jsonify(
            response= 'in_channel',
            text=response
            )

@app.route('/hitstreets', methods=['POST'])
def hitstreets():
    response= ("When you hit the streets to get what you need, name who you're going to and roll with their faction. Alternatively, roll the faction the MC tells you to. On a hit, you find what you need. On a 7-9, choose one below."
            "\n\n-Whoever you're going to is juggling their own problems."
            "\n-Whatever you need is more costly than anticipated.\n") +doFactionRoll()
    return jsonify(
            response= 'in_channel',
            text=response
            )


@app.route('/facetoname', methods=['POST'])
def facetoname():
    response= ("When you put a face to a name, or vice versa, roll with their Faction. On a hit, you know their reputation; the MC tells you what most people know about them."
            "On a 10+, you've dealt with them before; learn something interesting and useful about them or they owe you a debt. On a miss, you don't know them or you owe them; the MC will tell you which.\n") + doFactionRoll()
    return jsonify(
            response= 'in_channel',
            text=response
            )

@app.route('/investigate', methods=['POST'])
def investigate():
    response= ("When you investigate a place of power, roll with the Faction that owns it. On a hit, you see below the surace to the reallity underneath."
            "On a 10+, you can ask the MC one question about the schemes and politics of the faction in question.\n") +doFactionRoll()
    return jsonify(
            response= 'in_channel',
            text=response
            )


@app.route('/harm', methods=['POST'])
def harm():
    if not isRequestValid(request):
        abort(400)

    splitText = tokenize(request.form["text"])
    if (isInt(splitText[0])):
        addHarm(splitText[0])
    else:
        if splitText[0] == "set":
            setHarm(splitText[1:])
        elif splitText[0] == "get":
            pass
        else:
            return jsonify(
                    response_type='in_channel',
                    text="Invalid subcommand"
                    )
    return jsonify(
            response_type='in_channel',
            text=getHarm()
            )

@app.route('/heal', methods=['POST'])
def heal():
    if not isRequestValid(request):
        abort(400)
    splitText = tokenize(request.form["text"])
    if (isInt(splitText[0])):
        healHarm(splitText[0])
    else:
        return jsonify(
                response_type='in_channel',
                text="Invalid subcommand"
                )
    return jsonify(
            response_type='in_channel',
            text=getHarm()
            )

@app.route('/corruption', methods=['POST'])
def corruption():
    if not isRequestValid(request):
        abort(400)
    splitText = tokenize(request.form["text"])
    if (isInt(splitText[0])):
        addCorruption(splitText[0])
    else:
        if splitText[0] == "set":
            setCorruption(splitText[1:])
        elif splitText[0] == "get":
            pass
        else:
            return jsonify(
                    response_type='in_channel',
                    text="Invalid subcommand"
                    )
    return jsonify(
            response_type='in_channel',
            text=getCorruption()
            )

def addHarm(numHarm):
    harmAdded = 0
    query = "SELECT * FROM HARM WHERE ID = '" + request.form["user_id"] + "'"
    res = db.doSelect(query)
    if len(res) <= 0:
        initQuery = "REPLACE INTO HARM VALUES ('"
        initQuery += request.form["user_id"] + "'"
        initQuery += ", 0, 0, 0, 0, 0)"
        db.runQuery(initQuery)
        res = db.doSelect(query)
    harmArr = []
    for i in range(1, len(res[0])):
        if res[0][i] == 0 and int(harmAdded) < int(numHarm):
            harmArr.append(1)
            harmAdded += 1
        else:
            harmArr.append(res[0][i])
    setHarm(harmArr)

def healHarm(numHarm):
    healed = 0
    query = "SELECT * FROM HARM WHERE ID = '" + request.form["user_id"] + "'"
    res = db.doSelect(query)
    if len(res) <= 0:
        initQuery = "REPLACE INTO HARM VALUES ('"
        initQuery += request.form["user_id"] + "'"
        initQuery += ", 0, 0, 0, 0, 0)"
        db.runQuery(initQuery)
        res = db.doSelect(query)
    harmArr = []
    for i in range(1, len(res[0])):
        if res[0][i] == 1 and int(healed) < int(numHarm):
            harmArr.append(0)
            healed += 1
        else:
            harmArr.append(res[0][i])
    setHarm(harmArr)

def setHarm(harmArr):
    query = "REPLACE INTO HARM VALUES ('"
    query += request.form["user_id"] + "'"
    for i in range(0, 5):
        query += ", " + str(harmArr[i])
    query += ")"
    db.runQuery(query)

def getHarm():
    query = "SELECT * FROM HARM WHERE ID = '" + request.form["user_id"] + "'"
    res = db.doSelect(query)
    allHarm = True
    if not res:
        return "No data"
    retStr = db.getPlayer(str(res[0][0]))[Stats.NAME] +":"
    if res:
        for i in range(1, len(res[0])):
            char = ":white_square:"
            if res[0][i] != 0:
                char = ":negative_squared_cross_mark:"
            else:
                allHarm = False
            retStr += " " + char
    if allHarm:
        retStr += "\nSorry, you're dead! :sob:"
    return retStr

def addCorruption(numCorruption):
    query = "SELECT * FROM CORRUPTION WHERE ID = '" + request.form["user_id"] + "'"
    res = db.doSelect(query)

    if len(res) <= 0:
        initQuery = "REPLACE INTO CORRUPTION VALUES ('"
        initQuery += request.form["user_id"] + "'"
        initQuery += ", 0, 0)"
        db.runQuery(initQuery)
        res = db.doSelect(query)

    totalCorruption = int(res[0][1]) * 5 + int(res[0][2]) + int(numCorruption)
    setCorruption([int(totalCorruption / 5), totalCorruption % 5])

def setCorruption(corruptionArr):
    query = "REPLACE INTO CORRUPTION VALUES ('"
    query += request.form["user_id"] + "'"
    query += ", " + str(corruptionArr[0]) + ", " + str(corruptionArr[1])
    query += ")"
    db.runQuery(query)

def getCorruption():
    query = "SELECT * FROM CORRUPTION WHERE ID = '" + request.form["user_id"] + "'"
    res = db.doSelect(query)
    if len(res) <= 0:
        initQuery = "REPLACE INTO CORRUPTION VALUES ('"
        initQuery += request.form["user_id"] + "'"
        initQuery += ", 0, 0)"
        db.runQuery(initQuery)
        res = db.doSelect(query)
    retStr = db.getPlayer(str(res[0][0]))[Stats.NAME] +": "
    retStr += "Level " + str(res[0][1]) + ":"
    if res:
        for i in range(0, 5):
            char = ":white_square:"
            if i < res[0][2]:
                char = ":negative_squared_cross_mark:"
            retStr += " " + char
    return retStr

@app.route('/debt', methods=['POST'])
def debt():
    if not isRequestValid(request):
        abort(400)

    # extract command portion of the string
    splitText = tokenize(request.form["text"])
    command = splitText[0]

    subCommand = ""
    if (len(splitText) > 1):
        subCommand = splitText[1]

    playerData = db.getPlayer(request.form["user_id"])

    if playerData is None:
        return jsonify(
                response_type='in_channel',
                text="Player not found. Do /edit first!"
                )

    if command is None:
        return jsonify(
                response_type='in_channel',
                text="Subcommand not valid. Must be 'add', 'list', or 'delete'"
                )

    query = ""
    if command == "add":
        if len(request.form["text"][len(command) + 1 : len(request.form["text"])]) <= 5:
            return jsonify(
                    response_type='in_channel',
                    text="Debt text too short!"
                    )
        query = "INSERT INTO DEBTS (OWNER, TEXT) VALUES ('"
        query += request.form["user_id"] + "', '"
        query += request.form["text"][len(command) + 1 : len(request.form["text"])] + "')"
        db.runQuery(query)
        return jsonify(
                response_type='in_channel',
                text="Added debt!"
                )
    elif command == "delete":
        if len(request.form["text"][len(command) + 1 : len(request.form["text"])]) <= 0:
            return jsonify(
                    response_type='in_channel',
                    text="No debt id"
                    )
        db.runQuery("DELETE FROM DEBTS WHERE ID=" + request.form["text"][len(command) + 1 : len(request.form["text"])])
        return jsonify(
                response_type='in_channel',
                text="Deleted debt"
                )
    elif command == "list":
        debts = db.doSelect("SELECT * FROM DEBTS")
        response = ""
        for debt in debts:
            if (not subCommand):
                response += str(debt[0]) + ": " + db.getPlayer(str(debt[1]))[Stats.NAME] + " - " + debt[2]
                response += "\n"
            else:
                matchesAll = True
                for i in range(1, len(splitText)):
                    if (splitText[i].lower() not in (db.getPlayer(str(debt[1]))[Stats.NAME] + debt[2]).lower()):
                        matchesAll = False
                if matchesAll:
                    response += str(debt[0]) + ": " + db.getPlayer(str(debt[1]))[Stats.NAME] + " - " + debt[2]
                    response += "\n"

        return jsonify(
                response_type='in_channel',
                text=response
                )

    return jsonify(
            response_type='in_channel',
            text="Invalid subcommand"
            )

