import sqlite3
import os.path
import sys

DB_FILE = "db/urbanshadows.db"

class Database:
    def __init__(self):
        conn = sqlite3.connect(DB_FILE)
        cursor = conn.cursor()

        cursor.execute('''CREATE TABLE IF NOT EXISTS PLAYERS(
                          ID INT PRIMARY KEY         NOT NULL,
                          NAME               TEXT    NOT NULL,
                          BLOOD              INT     NOT NULL,
                          HEART              INT     NOT NULL,
                          MIND               INT     NOT NULL,
                          SPIRIT             INT     NOT NULL,
                          MORTALITY          INT     NOT NULL,
                          NIGHT              INT     NOT NULL,
                          POWER              INT     NOT NULL,
                          WILD               INT     NOT NULL,
                          HARM               INT,
                          HOLDS              INT
                          )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS NPCS(
                          ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                          NAME               TEXT          NOT NULL
                          )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS DEBTS(
                          ID INTEGER PRIMARY  KEY  AUTOINCREMENT NOT NULL,
                          OWNER               INT                NOT NULL,
                          TEXT                TEXT               NOT NULL,
                          FOREIGN KEY(OWNER)  REFERENCES         PLAYERS(ID)
                          )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS HARM(
                          ID INT PRIMARY        KEY                NOT NULL,
                          FAINT                 INT  DEFAULT 0     NOT NULL,
                          GRIEV1                INT  DEFAULT 0     NOT NULL,
                          GRIEV2                INT  DEFAULT 0     NOT NULL,
                          CRIT1                 INT  DEFAULT 0     NOT NULL,
                          CRIT2                 INT  DEFAULT 0     NOT NULL
                          )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS CORRUPTION(
                          ID INT PRIMARY        KEY DEFAULT 0      NOT NULL,
                          MAJOR                 INT DEFAULT 0      NOT NULL,
                          MINOR                 INT DEFAULT 0      NOT NULL
                          )''')
        conn.commit()
        conn.close()

    def runQuery(self, query):
        conn = sqlite3.connect(DB_FILE)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        conn.close()

    def doSelect(self, query):
        conn = sqlite3.connect(DB_FILE)
        cursor = conn.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        conn.close()
        return data

    def getPlayer(self, playerId):
        conn = sqlite3.connect(DB_FILE)
        cursor = conn.cursor()
        query = "SELECT * FROM PLAYERS WHERE ID = '" + playerId + "'"
        cursor.execute(query)
        data = cursor.fetchone()
        conn.close()
        if len(data) > 0:
            return data
        return ""
